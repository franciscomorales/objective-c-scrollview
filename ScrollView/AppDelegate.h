//
//  AppDelegate.h
//  ScrollView
//
//  Created by Francisco Morales on 8/31/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

