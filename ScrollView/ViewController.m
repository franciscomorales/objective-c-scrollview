//
//  ViewController.m
//  ScrollView
//
//  Created by Francisco Morales on 8/31/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIScrollView* scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollview.backgroundColor = [UIColor clearColor];
    [scrollview setPagingEnabled:YES];
    [self.view addSubview:scrollview];
    
    [scrollview setContentSize:CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height * 3)];
    
    UIView* redView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, scrollview.frame.size.width, scrollview.frame.size.height)];
    redView.backgroundColor = [UIColor redColor];
    [scrollview addSubview:redView];
    
    UIView* greenView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, 0, scrollview.frame.size.width, scrollview.frame.size.height)];
    greenView.backgroundColor = [UIColor greenColor];
    [scrollview addSubview:greenView];
    
    UIView* yellowView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, 0, scrollview.frame.size.width, scrollview.frame.size.height)];
    yellowView.backgroundColor = [UIColor yellowColor];
    [scrollview addSubview:yellowView];

    UIView* orangeView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height, scrollview.frame.size.width, scrollview.frame.size.height)];
    orangeView.backgroundColor = [UIColor orangeColor];
    [scrollview addSubview:orangeView];
    
    UIView* purpleView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height, scrollview.frame.size.width, scrollview.frame.size.height)];
    purpleView.backgroundColor = [UIColor purpleColor];
    [scrollview addSubview:purpleView];

    UIView* grayView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height, scrollview.frame.size.width, scrollview.frame.size.height)];
    grayView.backgroundColor = [UIColor grayColor];
    [scrollview addSubview:grayView];
    
    UIView* blackView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height*2, scrollview.frame.size.width, scrollview.frame.size.height)];
    blackView.backgroundColor = [UIColor blackColor];
    [scrollview addSubview:blackView];
    
    UIView* cyanView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height*2, scrollview.frame.size.width, scrollview.frame.size.height)];
    cyanView.backgroundColor = [UIColor cyanColor];
    [scrollview addSubview:cyanView];
    
    UIView* blueView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height*2, scrollview.frame.size.width, scrollview.frame.size.height)];
    blueView.backgroundColor = [UIColor blueColor];
    [scrollview addSubview:blueView];

    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
